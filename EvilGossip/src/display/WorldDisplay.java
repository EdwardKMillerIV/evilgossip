package display;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JPanel;

import sim.Denizen;
import sim.PlayerControlledMonster;
import sim.WorldSimulator;
import util.AnonFunc;
import util.Position2D;

public class WorldDisplay extends JPanel{

	private static final long serialVersionUID = 6643108147673787485L;
	private WorldSimulator ws;
	public WorldDisplay(WorldSimulator sim)
	{
		this.ws = sim;
		this.setPreferredSize(new Dimension((int)sim.getWorldExtent().getX(), (int)sim.getWorldExtent().getY()));
		this.setBackground(Color.white);
    	PlayerControlledMonster pcm = (PlayerControlledMonster)ws.getDenizenMap().getNearestOfTypeInRange(new Position2D(0.0, 0.0),100000000.0, new AnonFunc<Denizen>(){

			@Override
			public int function(Denizen param) {
				// TODO Auto-generated method stub
				if(param instanceof PlayerControlledMonster)
				return 1;
				return 0;
			}
    		
    	});
    	this.setFocusable(true);
    	this.addKeyListener(pcm);
	}
    @Override
	public void paintComponent(Graphics g)
    {
    	g.setColor(Color.red);
    	g.drawRect(0, 0, 120, 50);
    	int count = 0;
    	for(Denizen d : ws.getDenizenMap())
    	{
    		if(!d.getState().is("dead"))
    			count++;
    	}
    	g.drawString("Population: "+count, 3, 15);
    	g.drawString("Cities:"+ws.getHavenMap().size(), 3, 30);
    	ws.getRoadMap().render(g);
    	ws.getHavenMap().render(g);
    	ws.getDenizenMap().render(g);

    }
}
