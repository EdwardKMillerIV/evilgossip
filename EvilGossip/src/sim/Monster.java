package sim;

import java.awt.Color;
import java.awt.Graphics;

import behaviour.states.*;

import util.AnonFunc;
import util.Position2D;
import util.SmallUtils;

public class Monster extends Denizen {
	public static Monster RandomMonster(WorldSimulator worldSimulator) {
		Monster result = new Monster();
		result.location = new Position2D(worldSimulator.rng.nextDouble()*worldSimulator.getWorldExtent().getX(), 
				worldSimulator.rng.nextDouble()*worldSimulator.getWorldExtent().getY());
		result.properties.put("speed",20.0);
		result.changeState(new Stalking());
		return result;
	}

	@Override
	public void render(Graphics g) {
		if(!getBool("isAlive"))
			return;
		g.setColor(Color.green);
		g.fillOval((int)location.getX()-3, (int)location.getY()-3, 7, 7);
	}

	@Override
	public void start(WorldSimulator ws, Double delta) {
		
	}

	@Override
	public void move(WorldSimulator ws, Double delta) {
		this.stalk(ws, delta);
		super.move(ws, delta);
	}
	private void stalk(WorldSimulator ws, Double delta){
		Villager prey = null;
		double nearestD = Double.MAX_VALUE;
		double dist;
		for(Denizen d : ws.dm)
		{
			dist = d.location.subtract(this.location).getLength();
			if(d instanceof Villager && 
					d.location.subtract(this.location).getLength() < nearestD &&
					!d.getState().is("dead"))
			{
				//Can't assault villagers in towns
				if(!d.getState().is("sheltering"))
				{
					prey = (Villager)d;
					nearestD = dist;
				}
			}
		}
		if(prey != null)
		{
			this.location = location.add(SmallUtils.getVectorTowardPoint(location, prey.location, getNum("speed") * delta));
		}		
		else
		{
			System.out.println("No prey");
		}
	}
	@Override
	public void interact(WorldSimulator ws, Double delta) {
		Villager prey = (Villager)ws.dm.getNearestOfTypeInRange(location, 4.0, new AnonFunc<Denizen>(){
			@Override
			public int function(Denizen param) {
				if(param instanceof Villager)
				{
					Villager v = (Villager) param;
					if(!v.getState().is("dead") && !v.getState().is("sheltering"))
					return 1;
				}
				return 0;
			}
		});
		if(prey != null)
			prey.changeState(new Dead());
	}

	@Override
	public void finish(WorldSimulator ws, Double delta) {
		// TODO Auto-generated method stub
		
	}
}
