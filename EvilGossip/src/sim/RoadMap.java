package sim;

import java.awt.geom.Line2D;
import java.util.Collection;
import java.util.TreeSet;

import sim.Denizen;
import sim.Haven;

import util.Position2D;
import util.RenderableCollection;

public class RoadMap extends RenderableCollection<Road>{

	private static final long serialVersionUID = 8490098489955053714L;
	
	RenderableCollection<Haven> havens;

	public RoadMap(RenderableCollection<Haven> havens)
	{
		this.havens = havens;
	}
	public Road getNearestRoadToDenizen(Denizen d)
	{
		Road nearestR = null;
		Double nearestDistance = Double.MAX_VALUE;
		for(Road r : this)
		{
			Line2D lr = r.getLine();
			Double dist = lr.ptSegDist(d.location);
			if(dist < nearestDistance && r.inUse)
			{
				nearestR = r;
				nearestDistance = dist;
			}
		}
		return nearestR;
	}
	public void generateRoads()
	{
		Collection<Road> hroads = new TreeSet<Road>();
		for(Haven h : havens)
		{
			for(Haven z : havens)
			{
				if(h != z)
				{
					hroads.add(new Road(h,z));
				}
			}
		}
		for(Road r : hroads)
		{
			for(Road z : hroads)
			{
				if(r.inUse && r != z &&r.doesOverlap(z))
				{
					if(r.doesOverlap(z))
					{
						if(r.a == z.a || r.b == z.b || r.a == z.b || r.b == z.a)
						{
						}
						else
							z.inUse = false;
					}
					else
						z.inUse = false;
				}
				else
				{
					//System.out.println("foo");
				}
			}
		}
		this.clear();
		for(Road r : hroads)
		{
			if(r.inUse)
				this.add(r);
		}
	}
	public Double getNearestRoadDistanceToPosition(Position2D d) {
		Double nearestDistance = Double.MAX_VALUE;
		for(Road r : this)
		{
			Line2D lr = r.getLine();
			Double dist = lr.ptSegDist(d.x, d.y);
			if(dist < nearestDistance && r.inUse)
			{
				nearestDistance = dist;
			}
		}
		return nearestDistance;		
	}
	public Road getRandomNearbyRoad(Position2D d, double range)
	{
		RenderableCollection<Road> nearestR = new RenderableCollection<Road>();
		for(Road r : this)
		{
			Line2D lr = r.getLine();
			Double dist = lr.ptSegDist(d.x, d.y);
			if(dist < range && r.inUse)
			{
				nearestR.add(r);
			}
		}
		return nearestR.getRandomElement();
	}
	public Position2D getNearestRoadLocationToPosition(Position2D d) {
		Road nearestR = null;
		Double nearestDistance = Double.MAX_VALUE;
		for(Road r : this)
		{
			Line2D lr = r.getLine();
			Double dist = lr.ptSegDist(d.x, d.y);
			if(dist < nearestDistance && r.inUse)
			{
				nearestR = r;
				nearestDistance = dist;
			}
		}
		Position2D testPoint = new Position2D(d);
		Position2D pt1 = new Position2D(nearestR.a.location);
		Position2D pt2 = new Position2D(nearestR.b.location);
		// Find the point on a line defined by pt1 and pt2 that 
		// is nearest to a given point tp 
		//       tp 
		//       /| 
		//    A / | 
		//     /  | 
		//    /   | 
		//  pt1---o--------------pt2 
		//     B'      B 

		// Get the vectors between the points 
		Position2D A = testPoint.subtract(pt1); 
		Position2D B = pt2.subtract(pt1); 

		// Find the cos of the angle between the vectors 
		double cosTheta = A.dotProduct(B) / (A.getLength() * B.getLength()); 
		// Use that to calculate the length of B' 
		double BPrimeLength = A.getLength() * cosTheta; 
		// Find the ratio of the length of B' and B 
		double scale = BPrimeLength / B.getLength(); 
		// Scale B by that ratio 
		B.multiply(scale); 
		// Translate p1 by B, this puts it at o 
		Position2D C = pt1.add(B); 
		return C;	
	}
}
