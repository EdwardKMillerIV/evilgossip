package sim;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.geom.Line2D;

import behaviour.states.*;

import util.AnonFunc;
import util.Position2D;
import util.SmallUtils;

public class Villager extends Denizen {
	
	public static Villager RandomVillager(WorldSimulator worldSimulator) {
		Villager result = new Villager();
		result.location = SmallUtils.getRandomCartesianPositionWithinRadiusOfPoint(worldSimulator.hm.getRandomElement().location,20.0);
		result.assign("speed", 15.0);
		result.changeState(new Idle());
		return result;
	}

	@Override
	public void render(Graphics g) {
		if(getState().is("dead"))
			return;
		if(getState().is("sheltering"))
			g.setColor(Color.red);
		else if(getState().is("idle"))
			g.setColor(Color.pink);
		else if(getState().is("seeking"))
			g.setColor(Color.black);
		else
			g.setColor(Color.gray);
		g.drawOval((int)location.getX()-1, (int)location.getY()-1, 3, 3);
		if(this.debugPathfindLine != null)
			g.drawLine((int)debugPathfindLine.getX1(),
					(int)debugPathfindLine.getY1(),
					(int)debugPathfindLine.getX2(),
					(int)debugPathfindLine.getY2()
					);
	}

	@Override
	public void start(WorldSimulator ws, Double delta) {
	}

	@Override
	public void move(WorldSimulator ws, Double delta) {
		if(getState().is("dead"))
		{
			return;
		}
		else if(getState().is("sheltering"))
		{
			assign("speed",15.0);
			assign("stamina",1.0);	
		}
		else if(getState().is("panicking"))
		{
			assign("speed",25.0);
			assign("stamina", getNum("stamina") - (delta/25.0));
			if(getNum("stamina") < 0.0) assign("stamina", 0.0);
			panic(ws,delta);
		}
		else if(getState().is("idle"))
		{
			assign("speed",15.0);
			assign("stamina", getNum("stamina") + (delta/30.0));
			if(getNum("stamina") > 1.0) assign("stamina", 1.0);
			changeState(new Seeking());
			randomRoadWalk(ws,delta);	
		}
		else if(getState().is("seeking"))
		{
			if(getPos("currDestination")!= null)
			{
				if(getPos("currDestination").distance(location)<1.0)
				{
					changeState(new Sheltering());
				}
				else
				{
					randomRoadWalk(ws,delta);
				}
			}
			else
			{
				//TODO: Make this unnecessary!
				randomRoadWalk(ws,delta);
			}
		}
		getState().execute(this, ws);
		super.move(ws, delta);
	}
	private void panic(WorldSimulator ws, Double delta)
	{
		Haven h = ws.hm.getNearestOfTypeInRange(location, 100.0, new AnonFunc<Haven>(){
			@Override
			public int function(Haven param) {
				return 1;
			}
		});
		Monster m = (Monster) ws.dm.getNearestOfTypeInRange(location, 10.0, new AnonFunc<Denizen>(){
			@Override
			public int function(Denizen param) {
				if(param instanceof Monster) return 1;
				return 0;
			}	
		});
		Position2D vector = new Position2D();
		if(m == null) changeState(new Idle());
		if(m != null) vector = vector.add(SmallUtils.getVectorTowardPoint(location, m.location, 1.0));
		if(h != null) vector = vector.add(SmallUtils.getVectorTowardPoint(location, h.location, 1.0));
		if(Double.isNaN(vector.getLength())) return;
		location = location.add(vector.normalize().multiply(-1.0 * getNum("speed") * delta * getNum("stamina")));
	}
	private void randomRoadWalk(WorldSimulator ws, Double delta)
	{
		if(getPos("currDestination") == null || getPos("currDestination").subtract(location).getLength()<1.0)
		{
			Road r;
			if(getPos("currDestination") != null) r = ws.rm.getRandomNearbyRoad(getPos("currDestination"), 3.0);
			else  r = ws.rm.getRandomNearbyRoad(location, 50.0);
			if(r!=null)
			{
				double distA = r.a.location.subtract(location).getLength();
				double distB = r.b.location.subtract(location).getLength();
				assign("currDestination", distA>distB?r.a.location:r.b.location);
			}
			else
			{
				assign("currDestination",ws.hm.getRandomElement().location);
			}
		}
		Position2D vector = SmallUtils.getVectorTowardPoint(location, getPos("currDestination"), 10.0);
		vector = vector.add(SmallUtils.getVectorTowardPoint(location, ws.rm.getNearestRoadLocationToPosition(location), 1.0));
		vector = vector.normalize().multiply(getNum("speed") * delta * getNum("stamina"));
		
		//this.debugPathfindLine = new Line2D.Double(location.x, location.y, getPos("currDestination").x, getPos("currDestination").y);
		this.location = location.add(vector);		
	}
	@Override
	public void interact(WorldSimulator ws, Double delta) {
		// TODO Auto-generated method stub
		if(getState().is("dead"))
		{
			return;
		}
		Monster predator = (Monster)ws.dm.getNearestOfTypeInRange(location, 6.0, new AnonFunc<Denizen>(){
			@Override
			public int function(Denizen param) {
				if(param instanceof Monster)
				{
					if(param.getBool("isAlive")) return 1;
				}
				return 0;
			}
		});
		Haven safeplace = (Haven)ws.hm.getNearestOfTypeInRange(location, 6.0, new AnonFunc<Haven>(){
			@Override
			public int function(Haven param){
				return 1;
			}
		});
		if(predator != null)
		{
			//stealthiness
			if(predator.getState().is("prowling"))
			{
				double predatorstealthiness = 0.10;//10% chance of avoidance per second in detection range
				double chanceOfDetectingNow = Math.log(1.0-predatorstealthiness)/Math.log(1/delta);
				if(ws.rng.nextDouble() < chanceOfDetectingNow)
					changeState(new Panicking());
			}
			else
				changeState(new Panicking());
		}
		if(safeplace != null && predator != null)
		{
			changeState(new Sheltering());
		}
	}

	@Override
	public void finish(WorldSimulator ws, Double delta) {
		// TODO Auto-generated method stub
		
	}

}
