package sim;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.geom.Line2D;

import sim.Haven;

import util.Renderable;

public class Road implements Renderable, Comparable<Road>{

	@Override
	public void render(Graphics g) {
		g.setColor(Color.cyan);
		g.drawLine((int)a.location.getX(), (int)a.location.getY(), (int)b.location.getX(), (int)b.location.getY());
	}
	Haven a;
	Haven b;
	boolean inUse = true;
	public Road(Haven a, Haven b)
	{
		this.a = a;
		this.b = b;
	}
	public boolean doesOverlap(Road otherRoad)
	{
		Line2D thisRoad = new Line2D.Double(a.location, b.location);
		Line2D thatRoad = new Line2D.Double(otherRoad.a.location, otherRoad.b.location);
		if(thisRoad.intersectsLine(thatRoad))
			return true;
		return false;
	}
	public Double getLength()
	{
		return Math.sqrt(
	            Math.pow(a.location.getX() - b.location.getX(), 2) +
	            Math.pow(a.location.getY() - b.location.getY(), 2) );
	}
	public Line2D.Double getLine()
	{
		return new Line2D.Double(a.location, b.location);
		
	}
	@Override
	public int compareTo(Road o) {
		return (int)(this.getLength() - o.getLength());
	}
}