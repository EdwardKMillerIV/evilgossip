package sim;

import java.util.Random;

import util.Position2D;
import util.RenderableCollection;
import util.SmallUtils;

public class WorldSimulator {

	protected RoadMap rm;
	protected RenderableCollection<Haven> hm;
	protected RenderableCollection<Denizen> dm;
	private Double worldExtentX;
	private Double worldExtentY;
	public Random rng;
	
	
	public RenderableCollection<Road> getRoadMap() {return rm;}
	public RenderableCollection<Haven> getHavenMap() {return hm;}
	public RenderableCollection<Denizen> getDenizenMap() {return dm;}
	public Position2D getWorldExtent(){return new Position2D(worldExtentX, worldExtentY);}

	public WorldSimulator()
	{
		hm = new RenderableCollection<Haven>();
		dm = new RenderableCollection<Denizen>();
		rm = new RoadMap(hm);
		rng = new Random();
		worldExtentX = 1000.0;
		worldExtentY = 600.0;
	}
	public void populate(){
		int numHavens = 10;
		int numVillagers = 200;
		int numMonsters = 7;
		
		//Dot the landscape with places and beings
		for(int i = 0; i < SmallUtils.findMax(numHavens,numVillagers,numMonsters); i++)
		{
			if(i<numHavens)
				hm.add(Haven.RandomHaven(this));
			if(i<numVillagers)
				dm.add(Villager.RandomVillager(this));
			if(i<numMonsters)
				dm.add(Monster.RandomMonster(this));
		}
		dm.add(PlayerControlledMonster.RandomPCMonster(this));
		rm.generateRoads();
	}
	public void simulate(long delta) {
		Double dt = ((double) delta)/1000.0;
		for(Denizen joe : dm)
		{
			joe.start(this, dt);
		}
		for(Denizen joe : dm)
		{
			joe.move(this, dt);
		}
		for(Denizen joe : dm)
		{
			joe.interact(this, dt);
		}
		for(Denizen joe : dm)
		{
			joe.finish(this, dt);
		}
	}
	public static long currentTimeMillis(){
		return System.currentTimeMillis() * 1;
	}
}
