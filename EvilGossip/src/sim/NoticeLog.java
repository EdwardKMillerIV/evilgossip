package sim;

import java.util.ArrayList;

import util.Notice;

public class NoticeLog {
	private ArrayList<Notice> log;

	public NoticeLog()
	{
		log = new ArrayList<Notice>();
	}
	public void post(Notice n)
	{
		this.log.add(n);
	}
	//Returns an appropriate notice object for the highest priority notice of type event
	public Notice entails(String event)
	{
		Notice result = null;
		long currActive = Long.MIN_VALUE;
		for(Notice n : log)
		{
			if(n.isActive() > currActive && n.getEvent().equals(event))
			{
				currActive = n.isActive();
				result = n;
			}
		}
		//TODO: Expire stuff properly
		if(result != null && WorldSimulator.currentTimeMillis() - result.isActive() > 100)
			result = null;
		return result;
	}
}
