package sim;

import java.awt.Color;
import java.awt.Graphics;

import util.Position2D;
import util.Positionable;
import util.Renderable;

public class Haven extends Positionable implements Renderable{

	@Override
	public void render(Graphics g) {
		g.setColor(Color.blue);
		g.drawOval((int)location.getX()-2, (int)location.getY()-2, 5,5);
	}

	public static Haven RandomHaven(WorldSimulator worldSimulator) {
		// TODO Auto-generated method stub
		Haven result = new Haven();
		result.location = new Position2D(worldSimulator.rng.nextDouble()*worldSimulator.getWorldExtent().getX(), 
				worldSimulator.rng.nextDouble()*worldSimulator.getWorldExtent().getY());
		return result;
	}

}
