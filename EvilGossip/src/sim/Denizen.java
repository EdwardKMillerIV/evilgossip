package sim;

import java.awt.Graphics;
import java.awt.geom.Line2D;
import java.util.Map;
import java.util.TreeMap;

import behaviour.AIState;
import behaviour.Intelligent;

import util.Position2D;
import util.Positionable;
import util.Renderable;

public abstract class Denizen extends Positionable implements Renderable, Intelligent{

	public Line2D debugPathfindLine = null;
	public Map<String,Object> properties = new TreeMap<String,Object>();
	public Denizen()
	{
		properties.put("stamina", 1.0);
		properties.put("isAlive", true);
		properties.put("speed", 10.0);
		properties.put("currDestination", null);
	}
	//Properties
	public Double getNum(String s)
	{
		return (double) properties.get(s);
	}
	public Boolean getBool(String s)
	{
		return (boolean) properties.get(s);
	}
	public Position2D getPos(String s)
	{
		return (Position2D) properties.get(s);
	}
	public void assign(String s, Object o)
	{
		properties.put(s, o);
	}
	//Intelligent
	private AIState currentState = null;
	public void changeState(AIState newState){
		if(currentState != null)
		currentState.exit();
		currentState = newState;
		currentState.enter();
	}
	public AIState getState(){
		return currentState;
	}
	//Renderable
	@Override
	public abstract void render(Graphics g);
	//Denizen
	public abstract void start(WorldSimulator ws, Double delta);
	public void move(WorldSimulator ws, Double delta){		
		// Fix bounds
		this.location.x = Math.max(this.location.x, 0);
		this.location.y = Math.max(this.location.y, 0);
		this.location.x = Math.min(this.location.x, ws.getWorldExtent().getX());
		this.location.y = Math.min(this.location.y, ws.getWorldExtent().getY());
	}
	public abstract void interact(WorldSimulator ws, Double delta);
	public abstract void finish(WorldSimulator ws, Double delta);

}
