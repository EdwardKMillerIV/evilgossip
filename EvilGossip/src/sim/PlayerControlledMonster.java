package sim;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import util.Position2D;
import behaviour.states.Prowling;
import behaviour.states.Stalking;

public class PlayerControlledMonster extends Monster implements KeyListener{
	public static PlayerControlledMonster RandomPCMonster(WorldSimulator worldSimulator) {
		PlayerControlledMonster result = new PlayerControlledMonster();
		result.location = new Position2D(worldSimulator.rng.nextDouble()*worldSimulator.getWorldExtent().getX(), 
				worldSimulator.rng.nextDouble()*worldSimulator.getWorldExtent().getY());
		result.properties.put("speed",20.0);
		result.changeState(new Stalking());
		return result;
	}
	@Override
	public void render(Graphics g)
	{
		if(getState().is("prowling"))
			g.setColor(Color.DARK_GRAY);
		else
			g.setColor(Color.red);
		g.fillOval((int)location.x - 3, (int)location.y -3, 7, 7);
	}
	Position2D currentTransform = new Position2D(0.0,0.0);
	@Override
	public void move(WorldSimulator ws, Double delta) {
		location = location.transform(currentTransform.multiply(delta).multiply(getNum("speed")));
	}
	@Override
	public void keyTyped(KeyEvent e) {
	}
	@Override
	public void keyPressed(KeyEvent e) {
		char k = e.getKeyChar();
		if(k == 'w')
			this.currentTransform = this.currentTransform.add(new Position2D(0.0, -1.0));
		else if(k=='a')
			this.currentTransform = this.currentTransform.add(new Position2D(-1.0, 0.0));
		else if(k=='s')
			this.currentTransform = this.currentTransform.add(new Position2D(0.0, 1.0));
		else if(k=='d')
			this.currentTransform = this.currentTransform.add(new Position2D(1.0, 0.0));		
		else if(k=='q')
		{
			if(getState().is("prowling"))
			{
				assign("speed",20.0);
				changeState(new Stalking());
			}
			else
			{
				assign("speed",10.0);
				changeState(new Prowling());
			}
		}
	}
	@Override
	public void keyReleased(KeyEvent e) {
		char k = e.getKeyChar();
		if(k == 'w')
			this.currentTransform = this.currentTransform.add(new Position2D(0.0, 1.0));
		else if(k=='a')
			this.currentTransform = this.currentTransform.add(new Position2D(1.0, 0.0));
		else if(k=='s')
			this.currentTransform = this.currentTransform.add(new Position2D(0.0, -1.0));
		else if(k=='d')
			this.currentTransform = this.currentTransform.add(new Position2D(-1.0, 0.0));
		
	}
}
