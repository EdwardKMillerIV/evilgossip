package behaviour;

import sim.WorldSimulator;

public abstract class AIState {
	public abstract void enter();
	public abstract void execute(Intelligent actor, WorldSimulator environment);
	public abstract void exit();
	public String name(){
		return this.getClass().getSimpleName().toLowerCase();
	}
	public boolean is(String targetStateName) {
		return targetStateName.toLowerCase().equals(name());
	}
}
