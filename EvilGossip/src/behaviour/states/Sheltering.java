package behaviour.states;

import behaviour.AIState;
import behaviour.Intelligent;
import sim.WorldSimulator;

public class Sheltering extends AIState {
	private long shelteredAtTime;
	@Override
	public void enter() {
		// TODO Auto-generated method stub
		shelteredAtTime = WorldSimulator.currentTimeMillis();
	}

	@Override
	public void execute(Intelligent actor, WorldSimulator environment) {
		if(WorldSimulator.currentTimeMillis() - shelteredAtTime > 10000.0)
		{
			actor.changeState(new Idle());
		}
	}

	@Override
	public void exit() {

	}
}
