package behaviour.states;

import behaviour.AIState;
import behaviour.Intelligent;
import sim.WorldSimulator;

public class Idle extends AIState {
	@Override
	public void enter() {

	}

	@Override
	public void execute(Intelligent actor, WorldSimulator environment) {

	}

	@Override
	public void exit() {

	}
}
