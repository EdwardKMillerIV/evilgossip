package behaviour;


public interface Intelligent {
	public AIState getState();
	public void changeState(AIState newState);
}
