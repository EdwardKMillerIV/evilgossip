package util;

import java.awt.Graphics;

public interface Renderable {
	public void render(Graphics g);
}
