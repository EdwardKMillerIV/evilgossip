package util;

import java.awt.geom.Point2D;

//Position class with some vector functionality
public class Position2D extends Point2D {
	
	public double x;
	public double y;
	public Position2D(double d, double e) {
		this.setLocation(d,e);
	}

	public Position2D() {
		x = 0;
		y = 0;
	}

	public Position2D(Position2D there) {
		this.x = there.x;
		this.y = there.y;
	}

	public void transform(double dX, double dY)
	{
		this.setLocation(x+dX, y+dY);
	}

	@Override
	public double getX() {
		return this.x;
	}

	@Override
	public double getY() {
		return this.y;
	}

	@Override
	public void setLocation(double x, double y) {
		this.x = x;
		this.y = y;
	}

	public Position2D subtract(Position2D target) {
		return new Position2D(this.x - target.x, this.y - target.y);
	}

	public Position2D normalize() {
		if(this.getLength() == 0) return new Position2D(this);
		return new Position2D(this.x / this.getLength(), this.y / this.getLength());
	}
	
	public double getLength(){
		double result = Math.sqrt(Math.pow(this.x,2) + Math.pow(this.y, 2));
		return result;
	}

	public Position2D multiply(double range) {
		return new Position2D(this.x * range, this.y * range);
	}

	public Position2D add(Position2D target) {
		return new Position2D(this.x + target.x, this.y + target.y);
	}
	
	public double dotProduct(Position2D target){
		return this.x * target.x + this.y * target.y;
	}

	public Position2D transform(Position2D transform) {
		// TODO Auto-generated method stub
		return this.add(transform);
	}

}
