package util;

import java.util.List;

import sim.NoticeLog;


public interface Notifiable {
	public void postToSelf(Notice n);
	public NoticeLog getPosts();
}
