package util;

import java.awt.Graphics;
import java.util.ArrayList;

//List that provides some advanced access for elements that have positions
public class RenderableCollection<T extends Renderable> extends ArrayList<T>{

	private static final long serialVersionUID = 6368919148519467118L;

	public void render(Graphics g) {
		for(T foo : this)
		{
			foo.render(g);
		}
	}
	public T getRandomElement()
	{
		int index = SmallUtils.utilRNG.nextInt(this.size());
		if(index < this.size())
			return this.get(index);
		return null;
		
	}
	//The anonymous function must return 1+ if an object is of the desired class, 0- if otherwise.
	public T getNearestOfTypeInRange(Position2D origin, double range, AnonFunc<T> anonFunc) {
		T winner = null;
		double max = Double.MAX_VALUE;
		double candidate = 0;
		for(T e : this)
		{
			if(e instanceof Positionable && anonFunc.function(e) > 0)
			{
				if(((Positionable)e).location == null)
				{
					continue;
				}
				candidate = ((Positionable)e).location.distance(origin);
				if(candidate < max && candidate < range)
				{
					max = candidate;
					winner = e;
				}
			}
		}
		return winner;
	}

}
