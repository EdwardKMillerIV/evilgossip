package util;

import sim.Denizen;
import sim.Monster;

public class Notice {

	private final Denizen stimulus;
	private final long timestamp;
	private final String event;
	public Notice(String event, Denizen stimulator, long timestamp) {
		this.stimulus = stimulator;
		this.timestamp = timestamp;
		this.event = event;
	}
	
	public Denizen getStimulus(){
		return stimulus;
	}
	public long isActive() {
		// TODO Auto-generated method stub
		return timestamp;
	}

	public String getEvent() {
		// TODO Auto-generated method stub
		return event;
	}

}
