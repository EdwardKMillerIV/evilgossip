package util;

import java.util.Random;

public class SmallUtils {
	public static Random utilRNG = new Random();
	public static double findMax(double... vals) {
		   double max = Double.MIN_VALUE;

		   for (double d : vals) {
		      if (d > max) max = d;
		   }

		   return max;
		}
	public static Position2D getRandomCartesianPositionWithinRadiusOfPoint(Position2D center, Double range)
	{
		Position2D result = new Position2D();
		Double angle = utilRNG.nextDouble() * 2.0 * Math.PI;
		Double distance = utilRNG.nextDouble() * range;
		Double resultX = Math.cos(angle) * distance;
		Double resultY = Math.sin(angle) * distance;
		result.setLocation(resultX+center.getX(), resultY+center.getY());
		return result;
	}
	public static Position2D getRandomVectorWithinRadiusOfPoint(Position2D center, Double range)
	{
		Position2D result = new Position2D();
		Double angle = utilRNG.nextDouble() * 2.0 * Math.PI;
		Double distance = utilRNG.nextDouble() * range;
		Double resultX = Math.cos(angle) * distance;
		Double resultY = Math.sin(angle) * distance;
		result.setLocation(resultX+center.getX(), resultY+center.getY());
		return result;
	}
	public static Position2D getPositionTowardPoint(Position2D here, Position2D there, Double range)
	{
		return there.subtract(here).normalize().multiply(range).add(here);
	}
	public static Position2D getVectorTowardPoint(Position2D here, Position2D there, Double range)
	{
		return there.subtract(here).normalize().multiply(range);		
	}
}
