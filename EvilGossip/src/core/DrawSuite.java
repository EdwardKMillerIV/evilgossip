package core;

import java.awt.*; 
import java.applet.*;
import java.util.ArrayList;
import java.util.Collection;

import javax.swing.JFrame;
import javax.swing.JPanel;

import sim.RoadMap;

public class DrawSuite extends JPanel
{

 // Specify variables that will be needed everywhere, anytime here 
 // The font variable 
    Font bigFont;
    static long getSimulationTimeInMilliseconds()
    {
    	long speedMultiplier = 8;
    	return System.currentTimeMillis() * speedMultiplier;
    }
 // The colors you will use 
     Color redColor; 
     Color weirdColor; 
     Color bgColor;
     Collection<Denizen> population = new ArrayList<Denizen>();
     ArrayList<Haven> havens = new ArrayList<Haven>(); 
     RoadMap rm;
     MovementMap map;
	public double worldExtent = 600;
     public void init()  
     { 
    	 for(int i = 0; i < 12; i++)
    	 {
    		 this.havens.add(Haven.RandomHaven(this));
    	 }
    	 /*
    	 rm = new RoadMap(havens);
    	 rm.generateRoads();
    	 */
    	 map = new MovementMap(rm);
    	 for(int i = 0; i < 200; i ++)
    	 {
    		 this.population.add(Denizen.RandomDenizen(this));
    	 }
    	 this.population.add(Monster.RandomMonster(this));
    	 // Standard colors can be named like this 
          redColor = Color.red;

          // lesser known colors can be made with R(ed)G(reen)B(lue). 
          weirdColor = new Color(60,60,122);
          this.setPreferredSize(new Dimension(400,400));
          this.setSize(new Dimension(400,400));
  
          // this will set the backgroundcolor of the applet 
          setBackground(bgColor);
     }

     public void stop() 
     { 
     }
     static long lastTime = DrawSuite.getSimulationTimeInMilliseconds();
     public void simulate()
     {
    	 long currTime = DrawSuite.getSimulationTimeInMilliseconds();
    	 long time = currTime - lastTime;
    	 lastTime = currTime;
    	 for(Denizen joe : this.population)
    	 {
    		 if(!joe.isDead)
    		 {
	    		 joe.move(((double) time)/1000);
	    		 joe.interact(this.population);
	    		 joe.finish();	 
    		 }
    	 }
    	 for(Haven haven : havens)
    	 {
    		 haven.report();
    	 }    	 
     }

     //public void render(Graphics g)
     @Override
	public void paintComponent(Graphics g)
     {
    	  //g.clearRect(0, 0, (int)this.worldExtent+ 1000,(int)this.worldExtent+ 1000);
          g.drawRect(10,50,(int)this.worldExtent,(int)this.worldExtent);
    	 long currTime = DrawSuite.getSimulationTimeInMilliseconds();
    	 long time = currTime - lastTime;
    	 lastTime = currTime;
    	 /*
    	 for(RoadMap.Road road : this.rm.roads)
    	 {
    		 g.setColor(Color.cyan);
    		 g.drawLine((int)road.a.location.x+10,(int) road.a.location.y+50, (int)road.b.location.x+10, (int)road.b.location.y+50);
    	 }*/
    	 int currPop = 0;
    	 int currOvercurious = 0;
    	 int currDead = 0;
    	 for(Denizen joe : this.population)
    	 {
    		 if(!joe.isDead)
    			 currPop++;
    		 if(joe.scared > 0)
    			 g.setColor(Color.red);
    		 else if(joe.isDead)
    		 {
    			 g.setColor(Color.gray);
    			 currOvercurious += joe.adventurousness;
    			 currDead++;
    		 }
    		 else
    			 g.setColor(Color.black);
    		 if(joe instanceof Monster)
    		 {
    			 g.setColor(Color.green);
        		 g.fillOval((int)joe.location.x+10, (int)joe.location.y+50, 5, 5);
    		 }
    		 else if(joe instanceof Warden && !joe.isDead)
    		 {
    			 g.setColor(Color.orange);
    			 g.fillOval((int)joe.location.x+10,(int)joe.location.y+50,5,5);
    		 }
    		 if(!joe.isDevoured)
    			 g.drawOval((int)joe.location.x+10, (int)joe.location.y+50, 1, 1);
    		 if(joe.debugLine != null)
    			 g.drawLine(10+(int)joe.debugLine.x1, 50+(int)joe.debugLine.y1, 10+(int)joe.debugLine.x2, 50+(int)joe.debugLine.y2);
    		 
    	 }
    	 Double toofun = 0.0;
    	 if(currDead != 0)
    	 	toofun = (double)currOvercurious / (double)currDead;
    	 g.drawString(""+currPop, 0, 10);
    	 g.drawString(""+toofun, 75, 10);
    	 for(Haven haven : havens)
    	 {
    		 
    		 g.setColor(Color.blue);
    		 if(haven.occupants > 0) g.drawString(""+haven.occupants, (int)haven.location.x+10, (int)haven.location.y+50);
    		 g.drawOval((int)haven.location.x+10, (int)haven.location.y+50,3,3);
    	 }
     }
}
