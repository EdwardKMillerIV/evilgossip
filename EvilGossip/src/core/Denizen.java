package core;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.Collection;
import java.util.ArrayList;
import java.util.Random;

//import sim.RoadMap.Road;


public class Denizen {
	Point2D.Double location;
	static Random myRandom = new Random();
	Double speed = 100.0;
	long scared = -1;
	long willBeScared = -1;
	long oldnews = -1;
	int adventurousness = 3;
	boolean isDevoured = false;
	Double interactionDistance = 3.0;
	Double angle = myRandom.nextDouble() * 2 * Math.PI;
	Line2D.Double debugLine = null;
	Haven myHomeHaven;
	boolean isDead;
	int damageTaken;
	DrawSuite myDS;

	public static Denizen RandomDenizen(DrawSuite ds) {
		Denizen result = new Denizen(ds, myRandom.nextDouble() * ds.worldExtent,
				myRandom.nextDouble() * ds.worldExtent);
		return result;
	}

	public Denizen(DrawSuite ds, Double x, Double y) {
		location = new Point2D.Double(x, y);
		myDS = ds;
		myHomeHaven = ((ArrayList<Haven>) myDS.havens).get(myRandom
				.nextInt(myDS.havens.size()));
		this.adventurousness = 1 + myRandom.nextInt(5);
	}

	public void move(Double time) {
		Double changeX;
		Double changeY;
		if (this.scared > 0) {
			speed = 40.0;
			// * Nearest Haven

			Haven nearestHaven = null;
			Double nearestDist = Double.MAX_VALUE;

			for (Haven h : myDS.havens) {
				Double dist = Math.sqrt(Math.pow(
						h.location.x - this.location.x, 2)
						+ Math.pow(h.location.y - this.location.y, 2));
				if (dist < nearestDist) {
					nearestHaven = h;
					nearestDist = dist;
				}
			}
			// */
			/*
			 * Home haven Haven h = this.myHomeHaven; Double nearestDist =
			 * Math.sqrt( Math.pow(h.location.x - this.location.x, 2) +
			 * Math.pow(h.location.y - this.location.y, 2) ); Haven nearestHaven
			 * = h; //Double targetAngle = Math.atan((nearestHaven.location.x -
			 * this.location.x)/(nearestHaven.location.y - this.location.y));
			 * //changeX = speed * Math.cos(targetAngle) * time; //changeY =
			 * speed * Math.sin(targetAngle) * time;
			 */
			Double normX = (nearestHaven.location.x - this.location.x)
					/ nearestDist;
			Double normY = (nearestHaven.location.y - this.location.y)
					/ nearestDist;
			if (nearestDist < 10.0)
				this.interactionDistance = 10.0;
			changeX = speed * time * normX;
			changeY = speed * time * normY;
		} else {
			this.interactionDistance = 3.0;
			angle = angle + myRandom.nextDouble() * (Math.PI / 2)
					- (Math.PI / 4);
			speed = 20.0;
			changeX = speed * Math.sin(angle) * time;
			changeY = speed * Math.cos(angle) * time;
		}
		/*
		Road nearby = myDS.rm.getNearestRoadToDenizen(this);
		if (nearby.getLine().ptLineDist(this.location.x, this.location.y) > (this.adventurousness + 1.0)) {
			Line2D.Double near = nearby.getLine();
			Line2D.Double pointVector = new Line2D.Double(this.location.x,
					this.location.y, near.getX1(), near.getY1());
			Double pd = nearby.getLine().ptLineDist(this.location.x,
					this.location.y);
			// Double signed_angle =
			// Math.atan2(near.getY2()-near.getY1(),near.getX2()-near.getX1())
			// - Math.atan2(pointVector.getY2() -
			// pointVector.getY1(),pointVector.getX2() - pointVector.getX1());
			// changeX = speed * Math.cos(signed_angle) * time;
			// changeY = speed * Math.sin(signed_angle) * time;
			// changeX /= speed;
			// changeY /= speed;
			Point2D.Double intersect = locToSegment(
					(Point2D.Double) near.getP1(),
					(Point2D.Double) near.getP2(), new Point2D.Double(
							this.location.x, this.location.y));
			Double normX = (intersect.x - this.location.x) / pd;
			Double normY = (intersect.y - this.location.y) / pd;
			changeX = speed * normX * time;
			changeY = speed * normY * time;
			// this.debugLine = new Line2D.Double(this.location.x,
			// this.location.y, intersect.x, intersect.y);
		} else {
			this.debugLine = null;
		}
		this.location.setLocation(this.location.x + changeX, this.location.y
				+ changeY);
		// Fix bounds
		this.location.x = Math.max(this.location.x, 0);
		this.location.y = Math.max(this.location.y, 0);
		this.location.x = Math.min(this.location.x, myDS.worldExtent);
		this.location.y = Math.min(this.location.y, myDS.worldExtent);
		*/
	}

	public void interact(Collection<Denizen> others) {
		for (Denizen other : others) {
			Double dist = Math.sqrt(Math.pow(
					other.location.x - this.location.x, 2)
					+ Math.pow(other.location.y - this.location.y, 2));
			if (dist < this.interactionDistance) {
				// Scare the other if I'm scared and they've not encountered the
				// fear
				if (other.scared < 0
						&& this.scared > 0
						&& this.oldnews > DrawSuite.getSimulationTimeInMilliseconds()
						&& (other.oldnews > DrawSuite.getSimulationTimeInMilliseconds() || other.oldnews < 0)) {
					other.willBeScared = DrawSuite.getSimulationTimeInMilliseconds();
				}
				//Hurl rocks at monster from haven walls
				if (other instanceof Monster && this.isSafe())
				{
					other.hurt(1);
				}
			}
		}
	}

	public void hurt(int i) {
		// TODO Auto-generated method stub
		this.damageTaken += i;
	}

	public void finish() {
		if (willBeScared > 0) {
			this.scared = willBeScared;
			this.oldnews = this.scared + 10000;
		}

		if (DrawSuite.getSimulationTimeInMilliseconds() > this.oldnews) {
			this.willBeScared = -1;
			this.scared = -1;
		}
	}

	public static Denizen ScaredDenizen(DrawSuite ds) {
		Denizen result = Denizen.RandomDenizen(ds);
		result.scared = DrawSuite.getSimulationTimeInMilliseconds();
		result.willBeScared = result.scared;
		return result;
	}

	/**
	 * Wrapper function to accept the same arguments as the other examples
	 * 
	 * @param x3
	 * @param y3
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 * @return
	 */
	public static Point2D.Double locToSegment(double x3, double y3, double x1,
			double y1, double x2, double y2) {
		final Point2D.Double p3 = new Point2D.Double(x3, y3);
		final Point2D.Double p1 = new Point2D.Double(x1, y1);
		final Point2D.Double p2 = new Point2D.Double(x2, y2);
		return locToSegment(p1, p2, p3);
	}

	/**
	 * Returns the distance of p3 to the segment defined by p1,p2;
	 * 
	 * @param p1
	 *            First point of the segment
	 * @param p2
	 *            Second point of the segment
	 * @param p3
	 *            Point to which we want to know the distance of the segment
	 *            defined by p1,p2
	 * @return The distance of p3 to the segment defined by p1,p2
	 */
	public static java.awt.geom.Point2D.Double locToSegment(Point2D.Double p1,
			Point2D.Double p2, Point2D.Double p3) {

		final double xDelta = p2.getX() - p1.getX();
		final double yDelta = p2.getY() - p1.getY();

		if ((xDelta == 0) && (yDelta == 0)) {
			throw new IllegalArgumentException(
					"p1 and p2 cannot be the same point");
		}

		final double u = ((p3.getX() - p1.getX()) * xDelta + (p3.getY() - p1
				.getY()) * yDelta)
				/ (xDelta * xDelta + yDelta * yDelta);

		final Point2D.Double closestPoint;
		if (u < 0) {
			closestPoint = p1;
		} else if (u > 1) {
			closestPoint = p2;
		} else {
			closestPoint = new Point2D.Double(p1.getX() + u * xDelta, p1.getY()
					+ u * yDelta);
		}

		return closestPoint;
	}
	public boolean isSafe(){
		Haven nearestHaven = null;
		Double nearestDist = Double.MAX_VALUE;

		for (Haven h : myDS.havens) {
			Double dist = Math.sqrt(Math.pow(
					h.location.x - this.location.x, 2)
					+ Math.pow(h.location.y - this.location.y, 2));
			if (dist < nearestDist) {
				nearestHaven = h;
				nearestDist = dist;
			}
		}
		return nearestDist < 4.0;
	}
	public void die() {
		// TODO Auto-generated method stub
		this.isDead = true;
		this.scared = -1;
		this.oldnews = Long.MAX_VALUE;
		this.willBeScared = -1;
	}
}
