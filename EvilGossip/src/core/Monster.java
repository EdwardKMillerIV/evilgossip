package core;

import java.util.Collection;

public class Monster extends Denizen{
	public boolean hasEaten = false;
	private boolean hasKilled;
	public Monster(DrawSuite ds, Double x, Double y) {
		super(ds, x, y);
		interactionDistance = 5.0;
		speed = 5.0;
	}

	public static Monster RandomMonster(DrawSuite ds){
		Monster result = new Monster(ds, myRandom.nextDouble() * ds.worldExtent,
				myRandom.nextDouble() * ds.worldExtent);
		return result;
	}
	@Override
	public void move(Double time)
	{
		Double changeX;
		Double changeY;
		Denizen closest = null;
		Double distance = Double.MAX_VALUE;
		for(Denizen other : myDS.population)
		{
			//Seek out villagers
			if(other != this && !(other instanceof Monster) && !other.isDevoured && !other.isSafe())
			{
				Double dist = Math.sqrt(Math.pow(
						other.location.x - this.location.x, 2)
						+ Math.pow(other.location.y - this.location.y, 2));
				if(dist < distance)
				{
					closest = other;
					distance = dist;
				}
			}
		}
		if(closest == null) return;
		Double normX = (closest.location.x - this.location.x)/distance;
		Double normY = (closest.location.y - this.location.y)/distance;
		changeX = normX * speed * time;
		changeY = normY * speed * time;
		this.location.x += changeX;
		this.location.y += changeY;
	}
	@Override
	public void interact(Collection<Denizen> others)
	{
		for (Denizen other : others) {
			Double dist = Math.sqrt(Math.pow(
					other.location.x - this.location.x, 2)
					+ Math.pow(other.location.y - this.location.y, 2));
			if (this != other && dist < this.interactionDistance && !other.isDevoured && !other.isSafe()) {
				//Eat if able, scare otherwise
				if(other.isDead && !this.hasEaten && dist < 2.0)
				{
					this.hasEaten = true;
					other.isDevoured = true;
				}
				else if(!this.hasKilled && !other.isDead && dist < 2.0)
				{
					other.die();
					this.hasKilled = true;
				}
				else
				{
					other.willBeScared = DrawSuite.getSimulationTimeInMilliseconds();
					other.oldnews = DrawSuite.getSimulationTimeInMilliseconds()+3000;
				}
			}
		}		
	}
	@Override
	public void finish()
	{
		this.scared = -1;
		this.willBeScared = -1;
		this.oldnews = -1;
		this.hasEaten = false;
		this.hasKilled = false;
	}
}
