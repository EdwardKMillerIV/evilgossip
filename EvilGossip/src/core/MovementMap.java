package core;

import sim.RoadMap;
import util.Position2D;

public class MovementMap {

	public int width=800;
	public int height=800;
	private double[][] movemap;
	public MovementMap(RoadMap rm) {
		// TODO Auto-generated constructor stub
		movemap = new double[width][height];
		for(int i = 0; i < width; i++)
		{
			for(int j = 0; j < height; j++)
			{
				movemap[i][j] = rm.getNearestRoadDistanceToPosition(new Position2D(i,j));
			}
		}
	}
	public boolean isWalkable(int x, int y) {
		if(movemap[x][y] < 5.0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

}
