package core;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Collection;
import java.util.TreeSet;

public class RoadMap {
	Collection<Haven> havens;
	public Collection<Road> roads = new ArrayList<Road>();
	class Road implements Comparable<Road>{
		Haven a;
		Haven b;
		boolean inUse = true;
		public Road(Haven a, Haven b)
		{
			this.a = a;
			this.b = b;
		}
		public boolean doesOverlap(Road otherRoad)
		{
			Line2D thisRoad = new Line2D.Double(a.location.x, a.location.y, b.location.x, b.location.y);
			Line2D thatRoad = new Line2D.Double(otherRoad.a.location.x, otherRoad.a.location.y, otherRoad.b.location.x, otherRoad.b.location.y);
			if(thisRoad.intersectsLine(thatRoad))
				return true;
			return false;
		}
		public Double getLength()
		{
			return Math.sqrt(
		            Math.pow(a.location.x - b.location.x, 2) +
		            Math.pow(a.location.y - b.location.y, 2) );
		}
		public Line2D.Double getLine()
		{
			return new Line2D.Double(a.location.x, a.location.y, b.location.x, b.location.y);
			
		}
		@Override
		public int compareTo(Road o) {
			return (int)(this.getLength() - o.getLength());
		}
	}
	public RoadMap(Collection<Haven> havens)
	{
		this.havens = havens;
	}
	public Road getNearestRoadToDenizen(Denizen d)
	{
		Road nearestR = null;
		Double nearestDistance = Double.MAX_VALUE;
		for(Road r : this.roads)
		{
			Line2D lr = r.getLine();
			Double dist = lr.ptSegDist(d.location.x, d.location.y);
			if(dist < nearestDistance && r.inUse)
			{
				nearestR = r;
				nearestDistance = dist;
			}
		}
		return nearestR;
	}
	public void generateRoads()
	{
		Collection<Road> hroads = new TreeSet<Road>();
		for(Haven h : havens)
		{
			for(Haven z : havens)
			{
				if(h != z)
				{
					hroads.add(new Road(h,z));
				}
			}
		}
		for(Road r : hroads)
		{
			for(Road z : hroads)
			{
				if(r.inUse && r != z &&r.doesOverlap(z))
				{
					if(r.doesOverlap(z))
					{
						if(r.a == z.a || r.b == z.b || r.a == z.b || r.b == z.a)
						{
						}
						else
							z.inUse = false;
					}
					else
						z.inUse = false;
				}
				else
				{
					//System.out.println("foo");
				}
			}
		}
		this.roads.clear();
		for(Road r : hroads)
		{
			if(r.inUse)
				this.roads.add(r);
		}
	}
	public Double getNearestRoadDistanceToCoord(int i, int j) {
		// TODO Auto-generated method stub
		Road nearestR = null;
		Double nearestDistance = Double.MAX_VALUE;
		Point2D.Double d = new Point2D.Double((double)i, (double)j);
		for(Road r : this.roads)
		{
			Line2D lr = r.getLine();
			Double dist = lr.ptSegDist(d.x, d.y);
			if(dist < nearestDistance && r.inUse)
			{
				nearestR = r;
				nearestDistance = dist;
			}
		}
		return nearestDistance;		
	}
}
