package core;

import java.awt.BorderLayout;

import javax.swing.JFrame;

import sim.WorldSimulator;

import display.WorldDisplay;

public class MAIN {

	public static void main(String[] args) {
		JFrame myFrame = new JFrame();
		myFrame.setLayout(new BorderLayout());
		WorldSimulator sim = new WorldSimulator();
		sim.populate();
		WorldDisplay vv = new WorldDisplay(sim);
		myFrame.add(vv);
		myFrame.setVisible(true);
		myFrame.setPreferredSize(vv.getPreferredSize());
		myFrame.pack();
		long currTime = WorldSimulator.currentTimeMillis();
		long lastSimulate = currTime;
		long lastRender = currTime;
		
		while(true)
		{
			currTime = WorldSimulator.currentTimeMillis();
			if(currTime-lastSimulate > 1.0/1000.0)
			{
				sim.simulate(currTime-lastSimulate);
				lastSimulate = currTime;
			}
			if(currTime-lastRender > 1.0/30.0)
			{
				lastRender = currTime;
				myFrame.repaint();
			}
		}
	}
}
