package core;

import java.awt.geom.Point2D;
import java.util.Random;

public class Haven {

	static Random r = new Random();
	Point2D.Double location;
	int occupants = 0;
	DrawSuite myDS;
	public Haven(DrawSuite ds)
	{
		myDS = ds;
	}
	public static Haven RandomHaven(DrawSuite ds) {
		// TODO Auto-generated method stub
		Haven result = new Haven(ds);
		result.location = new Point2D.Double(r.nextDouble() * ds.worldExtent, r.nextDouble()*ds.worldExtent);
		return result;
	}
	public void report()
	{
		int counter = 0;
		for(Denizen h : myDS.population)
		{
			Double dist = Math.sqrt(
		            Math.pow(h.location.x - this.location.x, 2) +
		            Math.pow(h.location.y - this.location.y, 2) );	
			if(dist < 5.0 && h.scared > 0)
				counter++;
		}
		this.occupants = counter;
	}

}
