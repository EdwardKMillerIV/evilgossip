package core;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.Collection;
import java.util.Deque;

import util.AStar;

public class Warden extends Denizen{
	long alerted = -1;
	Haven targetHaven = null;
	Deque<Integer> currPath;
	Point2D.Double currStep;
	public Warden(DrawSuite ds, Double x, Double y) {
		super(ds, x, y);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void move(Double time)
	{
		Double changeX;
		Double changeY;
		if(this.alerted > 0)
		{
			Denizen closest = null;
			Double distance = Double.MAX_VALUE;
			for(Denizen other : myDS.population)
			{
				//Seek out villagers
				if(other != this && !(other instanceof Monster) && !other.isDevoured && !other.isSafe() && other.scared <0)
				{
					Double dist = Math.sqrt(Math.pow(
							other.location.x - this.location.x, 2)
							+ Math.pow(other.location.y - this.location.y, 2));
					if(dist < distance)
					{
						closest = other;
						distance = dist;
					}
				}
			}
			if(closest != null)
			{
				Double normX = (closest.location.x - this.location.x)/distance;
				Double normY = (closest.location.y - this.location.y)/distance;
				speed = 25.0;
				changeX = normX * speed * time;
				changeY = normY * speed * time;
				this.debugLine = new Line2D.Double(this.location, closest.location);
			}
			else
			{
				//System.out.println("something wrong");
				changeX = 0.0;
				changeY = 0.0;
			}
		}
		else
		{

			this.debugLine = null;
			if(this.targetHaven == null)
				this.targetHaven = myDS.havens.get(myRandom.nextInt(myDS.havens.size()));
			Double dist = Math.sqrt(Math.pow(
					this.targetHaven.location.x - this.location.x, 2)
					+ Math.pow(this.targetHaven.location.y - this.location.y, 2));
			while (dist < 10.0)
			{
				this.targetHaven = myDS.havens.get(myRandom.nextInt(myDS.havens.size()));
				dist = Math.sqrt(Math.pow(
						this.targetHaven.location.x - this.location.x, 2)
						+ Math.pow(this.targetHaven.location.y - this.location.y, 2));
				this.currPath = null;
				this.currStep = null;
			}
			if(this.currPath == null)
			{
				AStar alg = new AStar(this.myDS.map, (int)this.location.x, (int)this.location.y, (int)this.targetHaven.location.x, (int)this.targetHaven.location.y);
				if(alg.findPath())
				{
					this.currPath = alg.getPath();
				}
			}
			if(this.currStep == null && this.currPath != null)
				this.currStep = new Point2D.Double((double)this.currPath.poll(), (double)this.currPath.poll());
			Double nearness = dist;
			if(currStep != null)
			{
				nearness = this.location.distance(currStep);
				//If we're near the node, start out for the next one.
				if(nearness < 0.5 && this.location.distance(targetHaven.location) > 1.0)
				{
					this.currStep = new Point2D.Double((double)this.currPath.poll(), (double)this.currPath.poll());
					nearness = this.location.distance(currStep);
				}
			}
			Double normX;
			Double normY;
			if(currPath == null || currStep == null)
			{
				normX = (this.targetHaven.location.x - this.location.x)
					/ dist;
				normY = (this.targetHaven.location.y - this.location.y)
					/ dist;
			}
			else
			{
				normX = (currStep.x - this.location.x)/nearness;
				normY = (currStep.y - this.location.y)/nearness;
			}
			speed = 5.0;
			changeX = speed * normX * time;
			changeY = speed * normY * time;

		}
		this.location.x += changeX;
		this.location.y += changeY;		
		// Fix bounds
		this.location.x = Math.max(this.location.x, 0);
		this.location.y = Math.max(this.location.y, 0);
		this.location.x = Math.min(this.location.x, myDS.worldExtent);
		this.location.y = Math.min(this.location.y, myDS.worldExtent);
	}
	@Override
	public void interact(Collection<Denizen> others)
	{
		for (Denizen other : others) {
			Double dist = Math.sqrt(Math.pow(
					other.location.x - this.location.x, 2)
					+ Math.pow(other.location.y - this.location.y, 2));
			if (dist < 7.0 && other instanceof Monster)
			{
				this.alerted = DrawSuite.getSimulationTimeInMilliseconds();
			}
			else if(dist < this.interactionDistance)
			{
				other.willBeScared = DrawSuite.getSimulationTimeInMilliseconds();
				other.oldnews = DrawSuite.getSimulationTimeInMilliseconds() + 9000;
			}
		}		
	}
	@Override
	public void finish()
	{
		this.scared = -1;
		this.willBeScared = -1;
		this.oldnews = -1;
		if(DrawSuite.getSimulationTimeInMilliseconds() - this.alerted > 6000)
			this.alerted = -1;
	}

	public static Warden RandomWarden(DrawSuite ds) {
		// TODO Auto-generated method stub
		Warden result = new Warden(ds, myRandom.nextDouble() * ds.worldExtent,
				myRandom.nextDouble() * ds.worldExtent);
		return result;
	}
}
